import java.io.*;
import java.net.*;

public class server{
  
  public static void main(String[] args) throws Exception
  {
    server getReq = new server();
    
    getReq.SendReq("localhost", 80);
    
  }
  
  public void SendReq(String url, int port) throws Exception
  {
    Socket s = new Socket(url, port);
    
    PrintWriter wtr  = new PrintWriter(s.getOutputStream());
    
    
    wtr.println("GET / HTTP/1.1");
    wtr.println("localhost");
    wtr.println("");
    wtr.flush();
    
    BufferedReader bufRead = new BufferedReader(new InputStreamReader(s.getInputStream()));
    String outStr;
    
    while((outStr = bufRead.readLine()) != null)
    {
      System.out.println(outStr);
    }
    
    bufRead.close();
    wtr.close();
  }
}
